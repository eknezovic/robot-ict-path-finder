import sys

from enum import Enum
import numpy as np

from mazelib import Maze
from mazelib.generate.Prims import Prims
from mazelib.solve.BacktrackingSolver import BacktrackingSolver
from mazelib.solve.ShortestPath import ShortestPath


path_int_to_str = {
    0: " ",
    1: "X",
    2: "0",
    3: "F",
    4: "+",
}

path_str_to_int = {v: k for k, v in path_int_to_str.items()}


def generate_maze_grid(x, y):
    m = Maze()
    m.generator = Prims(x, y)  # TODO add different generators as CLI options
    m.generate()
    m.generate_entrances()
    m.grid[m.start] = 2
    m.grid[m.end] = 3
    m.solver = ShortestPath()
    return m.grid


import click


@click.command()
@click.option("-x", default=10, help="Number of rows will be 2*x+1")
@click.option("-y", default=10, help="Number of columns will be 2*y+1")
def main(x, y):
    grid = generate_maze_grid(x, y)
    np.savetxt(sys.stdout, grid.astype(int), fmt="%d", delimiter=" ", newline="\n")


if __name__ == "__main__":
    main()
