from enum import IntEnum


class PathEnum(IntEnum):
    EMPTY = 0
    WALL = 1
    START = 2
    END = 3
    TRAVERSED = 4
