import sys
import numpy as np

import click

from enums import PathEnum

path_int_to_str = {
    PathEnum.EMPTY.value: " ",
    PathEnum.WALL.value: "X",
    PathEnum.START.value: "0",
    PathEnum.END.value: "F",
    PathEnum.TRAVERSED.value: "+",
}

path_str_to_int = {v: k for k, v in path_int_to_str.items()}


@click.group()
def converters():
    pass


@click.command()
def to_int():
    for line in sys.stdin:
        numerical_items = [str(path_str_to_int[i]) for i in line.strip("\n")]
        new_line = " ".join(numerical_items) + "\n"
        print(new_line)


@click.command()
def to_text():
    grid = np.loadtxt(sys.stdin, dtype=int, delimiter=" ")
    for row in grid:
        items = [path_int_to_str[i] for i in row]
        new_line = "".join(items)
        print(new_line)


converters.add_command(to_int)
converters.add_command(to_text)

if __name__ == "__main__":
    converters()
