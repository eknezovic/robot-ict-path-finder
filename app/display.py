import sys
import numpy as np

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap


def create_display(grid):
    """Generate a simple image of the maze."""
    if grid.max() == 1:
        colors = ["xkcd:light yellow", "black"]
    elif grid.max() == 2:
        colors = ["xkcd:light yellow", "black", "xkcd:light red"]
    else:
        colors = [
            "xkcd:light yellow",
            "black",
            "xkcd:blue",
            "xkcd:green",
            "xkcd:light red",
        ]
    cmap = ListedColormap(colors)
    plt.figure(figsize=(10, 5))
    plt.imshow(
        grid,
        cmap=cmap,
        interpolation="nearest",
    )
    plt.xticks([]), plt.yticks([])
    return plt


def main():
    grid = np.loadtxt(sys.stdin, dtype=int, delimiter=" ")
    create_display(grid).savefig(sys.stdout.buffer)


if __name__ == "__main__":
    main()
