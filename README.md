
# RobotICT task description

There's a maze defined as a text file, 
with the dimensions X * Y (it can be any maze of any size) 
made of the following characters:
- "X" - wall
- " " - road
- "0" - starting point
- "F" - finish

Write a program in python app/which is able to find the shortest path 
through the maze from the point marked with the "0" character 
and finishing at the point marked with "F".

Print out the shortest path found as the result, on the screen.

# SETUP

Commands:

    docker build -t ek_robot .
    docker run -it --env-file ./example.env -v $PWD/examples/:/app/examples ek_robot bash

**All of the specified examples need to be run from inside of docker container!** (as specified above)

# The solution:

Consisted of multiple smaller programs communicating via UNIX pipes.
## QUICKSTART 

For the standard text display:

    cat examples/example6.txt | python app/convert.py to-int | python app/solve_neo4j.py | python app/convert.py to-text

For nicer display, it generates PNG file with matplotlib:

    cat examples/example6.txt | python app/convert.py to-int | python app/solve_neo4j.py | python app/display.py > examples/out.png

### generate.py

Used to generate.py a map. Outputs it in numpy friendly format

Example usage:

    python app/generate.py -x 15 -y 15

### convert.py

Via STDIN, it converts from numpy friendly format to the text format specified in task description. Or vice versa.

Example usage:

    cat examples/example2.txt | python app/convert.py to-int 
    python app/generate.py -x 15 -y 15 | python app/convert.py to-text

### solve_neo4j.py

From STDIN, it reads 2D array (type: int) map representation that can be easily read by numpy. 

It returns to STDOUT the same map but with the shortest path...

It uses neo4j built-in function to find the shortest path.

Couple of steps are involved here:
- Convert text from STDIN to numpy 2D array
- Convert 2D array to graph
- Connect to cloud (free tier) neo4j db instance:
    - Delete all previous data from db
    - Bulk load all graph nodes and relationships to db
    - Get the shortest path
- Update the map (2D array) with the shortest path
- Print the updated map to STDOUT

Example usage:

    python app/generate.py -x 20 -y 20 | python app/solve_neo4j.py | python app/convert.py to-text

**NOTE:** I experienced errors when setting -x and -y params greater than 20.

### display.py

Takes the numpy friendly map representation and displays it using matplotlib.

Example usage:

    cat examples/example2.txt | python app/convert.py to-int | python app/display.py > examples/out.png 
    cat examples/example2.txt | python app/convert.py to-int | python app/solve_neo4j.py | python app/display.py > examples/out.png 

