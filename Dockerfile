FROM python:3.8.10-slim

COPY ./app /app/app
COPY ./requirements.txt /app

WORKDIR /app

RUN apt-get update -y
RUN apt-get install python3-tk -y

RUN pip3 install --upgrade pip
RUN pip3 install setuptools
RUN pip3 install --no-cache-dir -r requirements.txt

# CMD exec gunicorn --bind :$PORT --workers 1 --threads 8 --timeout 0 -k uvicorn.workers.UvicornWorker src.main:app 
# CMD python app/main.py
CMD echo "Done!"
