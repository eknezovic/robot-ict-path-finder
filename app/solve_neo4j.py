import os
import sys
import time

import logging

import numpy as np

import networkx as nx
from neo4j import GraphDatabase

from enums import PathEnum

from dotenv import load_dotenv

load_dotenv()


def init_db_driver():
    # NEO4J
    NEO4J_USERNAME = os.getenv("NEO4J_USERNAME")
    # Dev DB
    NEO4J_URI = os.getenv("NEO4J_URI")
    NEO4J_PASSWORD = os.getenv("NEO4J_PASSWORD")

    # Create a new Driver instance
    driver = GraphDatabase.driver(NEO4J_URI, auth=(NEO4J_USERNAME, NEO4J_PASSWORD))

    driver.verify_connectivity()
    return driver


def get_total_node_count(tx):
    query = """
    MATCH (n) 
    RETURN COUNT(n) AS count
    """
    query_result = tx.run(query)
    for row in query_result:
        result = row["count"]
    return result


def get_shortest_path(tx, start, end):
    query = """
    MATCH (a: Location {x: $start_x, y: $start_y})
    MATCH (b: Location {x: $end_x, y: $end_y})
    MATCH p=shortestPath((a)-[r:GOES_TO*]->(b)) 
    WITH p
    WHERE length(p) > 1
    RETURN p
    """
    start_x, start_y = start
    end_x, end_y = end
    start_time = time.time()
    query_result = tx.run(
        query,
        start_x=int(start_x),
        start_y=int(start_y),
        end_x=int(end_x),
        end_y=int(end_y),
    )
    try:
        result = next(query_result)
    except StopIteration:
        raise ValueError("get_shortest_path query didn't return anything!")
    shortest_path_nodes = []
    for node in result["p"].nodes:
        node_tuple = node["x"], node["y"]
        if node_tuple not in [start, end]:
            shortest_path_nodes.append(node_tuple)
    return shortest_path_nodes


def bulk_load_to_db(tx, edges):
    query = """
        UNWIND $mapEntry AS item
        MERGE (a: Location {x: item["ax"], y: item["ay"]})
        MERGE (b: Location {x: item["bx"], y: item["by"]})
        MERGE (a)-[:GOES_TO]->(b)
        MERGE (a)<-[:GOES_TO]-(b)
        RETURN a, b;
    """
    l = []
    for a, b in edges:
        ax, ay = a
        bx, by = b
        item = {"ax": ax, "ay": ay, "bx": bx, "by": by}
        l.append(item)
        # tx.run(query, ax=ax, ay=ay, bx=bx, by=by)
    tx.run(query, mapEntry=l)


def delete_all_nodes_from_db(tx):
    query = """
    MATCH (n) DETACH DELETE n;
    """
    tx.run(query)


def neo4j_solve(driver, graph, start, end):
    with driver.session() as session:
        with session.begin_transaction() as tx:
            delete_all_nodes_from_db(tx)

            start_time = time.time()
            bulk_load_to_db(tx, graph.edges())
            print(f"Time to load to db: {time.time() - start_time} s", file=sys.stderr)

            start_time = time.time()
            result = get_shortest_path(tx, start, end)
            print(f"Time to solve: {time.time() - start_time} s", file=sys.stderr)

            tx.commit()
    return result


def find_indexes_of_only_item_in_grid(grid, item):
    indexes = list(zip(*np.where(grid == item)))
    if len(indexes) == 0:
        raise ValueError(f"Item {item} is not in grid!")
    elif len(indexes) > 1:
        raise ValueError(f"Multiple items {item}... There can be only one!")
    else:
        return indexes[0]


def update_grid_with_traversed_nodes(grid, traversed_nodes):
    for node in traversed_nodes:
        grid[node] = PathEnum.TRAVERSED.value


def get_graph_from_grid(grid):
    G = nx.grid_2d_graph(*grid.shape)
    for val, node in zip(grid.ravel(), sorted(G.nodes())):
        if val == PathEnum.WALL.value:
            G.remove_node(node)
    return G


def main():
    grid = np.loadtxt(sys.stdin, dtype=int, delimiter=" ")
    start_point = find_indexes_of_only_item_in_grid(grid, PathEnum.START.value)
    end_point = find_indexes_of_only_item_in_grid(grid, PathEnum.END.value)

    G = get_graph_from_grid(grid)
    driver = init_db_driver()

    shortest_path_nodes = neo4j_solve(driver, G, start_point, end_point)
    update_grid_with_traversed_nodes(grid, shortest_path_nodes)

    np.savetxt(sys.stdout, grid.astype(int), fmt="%d", delimiter=" ", newline="\n")


if __name__ == "__main__":
    main()
